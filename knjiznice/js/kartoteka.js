var oseba = [];
oseba = 
[
	{
		time: "2017-02-02",
		allergy: "Weeds",
		body_temperature: 38,
		temperature_unit: "°C",
		height: 180,
		height_unit: "cm",
		weight: 70,
		weight_unit: "kg",
		pulse: 100,
		pulse_unit: "/min"
	},
	{
		time: "2017-05-04",
		allergy: "Cows milk",
		body_temperature: "36",
		temperature_unit: "°C",
		height: 200,
		height_unit: "cm",
		weight: 90,
		weight_unit: "kg",
		pulse: 110,
		pulse_unit: "/min"
	},
	{
		time: "2014-11-10",
		allergy: "Egg yolks",
		body_temperature: "37.7",
		temperature_unit: "°C",
		height: 210,
		height_unit: "cm",
		weight: 100,
		weight_unit: "kg",
		pulse: 88,
		pulse_unit: "/min"
	}
]