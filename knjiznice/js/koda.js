
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}
var pacient = "";

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta, callback) {
  var ehrId = "";
  console.log("B");
  if (stPacienta == 1)
  {
      makeEHR("Kasper","duh","1995-01-05", function(id) {
          callback(id);
      });
  }
  else if (stPacienta == 2)
  {
      makeEHR("Ursa","Janzar","1966-05-10", function(id) {
         callback(id);
      });
  }
  else if (stPacienta == 3)
  {
      makeEHR("Andraz","Groznik","1992-12-08", function(id) {
          callback(id);
      });
  }
  else
  {
        
  }

  return ehrId;
}
function makeEHR(ime, priimek, datum, callback) {
    var sID = getSessionId();
    var napaka = "EHR se ni uspel generirati";
    if (!ime || !priimek || !datum || ime.trim().length == 0 || priimek.trim().length == 0 || datum.trim().length == 0)
    {
        callback(napaka);
    }
    else
    {
        $.ajaxSetup({
            headers: {"Ehr-Session": sID}
        });
        $.ajax({
            url: baseUrl + "/ehr",
            type: 'POST',
            success: function (data) {
              var ehr = data.ehrId;
              var partyData = {
                  firstNames: ime,
                  lastNames: priimek,
                  dateOfBirth: datum,
                  partyAdditionalInfo: [{key: "ehrId", value: ehr}]
              };
              $.ajax({
                  url: baseUrl + "/demographics/party",
                  type: 'POST',
                  contentType: 'application/json',
                  data: JSON.stringify(partyData),
                  success: function (party) {
                      if (party.action == 'CREATE') {
                          callback(ehr);
                      }
                  }
              });
            }
        });
    }
}


function generiranjePodatkov()
{
        var el = "<select id='iid' class='form-control' onchange='pacient1(this.value)'>\
     <option value='0'>Izbira</option>\
     </select>";
      $("#izbira").remove();
      $("#select").empty();
     $("#select").append(el);
     $("#status").html("EHRID: <br>");
     for (var i = 1; i <= 3; i++) {
         generirajPodatke(i, function(id){
            generiranjeIzbr(id, function(opa) {
                 $("#iid").append(opa);
                 $("#status").append(id+"<br>");
            });
         });
     }
}

function pacient1(aa) {
     if (aa != 0) {
         pacient = aa;
         $("#vnosEhrId").aa(pacient);
     } else {
         pacient = "";
     }
 }

function generiranjeIzbr(id, callback) {
     podatki(id, function(party) {
         $("#status").append(party.firstNames + " " + party.lastNames+ ":"+"<br>");
          callback("<option value='"+id+"'>"+ party.firstNames + " " + party.lastNames+"</option>");
     });
}
function podatki(ehrId, callback) {
    var sessionId = getSessionId();
     $.ajax({
 		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
 		type: 'GET',
 		headers: {"Ehr-Session": sessionId},
     	success: function (data) {
 			var party = data.party;
 			callback(party);
 		}
	});
     
}
function naprej() {
    var id = $("#vnosEhrId").val();
    if (id != null)
    {
        pacient = id;
        upload();
    }
    else
    {
        pacient = "";
    }
}

function upload()
{
    branjeEhrja(function(party){
       $(".podatki").html(party.firstNames+ " "+party.lastNames); 
    }); 
    
     
     var visina, teza, utrip;
     
     preberi("height", function(el) {
         if(el.length > 0) {
             $("#height1").html("NULL");
             
         } else {
             $("#height1").html(el[0].height);
             visina = el[0].height;
         }
     });
     
     preberi("weight", function(el) {
         if(el.length > 0) {
             $("#teza").html("NULL");
         } else {
             $("#teza").html(el[0].weightres[0].unit);
             teza = el[0].weight;
             document.getElementById('teza').value = teza; 
         }
     });
     
    
     preberi("blood_pressure", function(el) {
         if(el.length == 0) {
             $("#utrip").html("NULL");
         } else {
             $("#utrip").html("a");
             utrip = el[0].blood_pressure;
         }
     });
}
function branjeEhrja(callback)
{
    var id = getSessionId();
    var Ehr = pacient;
    if (!Ehr && Ehr.trim().length != 0)
    {
       $.ajax({
 			url: baseUrl + "/demographics/ehr/" + Ehr + "/party",
 			type: 'GET',
			headers: {"Ehr-Session": id},
        	success: function (data) {
 				var party = data.party;
 				callback(party);
 			}
 		}); 
    }
}


 
 function preberi(type, callback) {
 	var session = getSessionId();
  
 	var id = pacient;
  
 	$.ajax({
 		    url: baseUrl + "/view/" + id + "/" + type,
 		    type: 'GET',
 		    headers: {"Ehr-Session": session},
 		    success: function (el) {
 		        callback(el);
 		    },
 		    error: function() {
 		    	callback("Napaka!");
 		    }
 		});
 }

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
